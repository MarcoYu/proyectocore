package model;

public class Operacion {
	
	private int num1, num2;
	
	public Operacion (int n, int m) {
		
		num1 =n;
		num2 = m;
	}
	
	public int sumar() {
		
		return num1+num2;
	}

}
