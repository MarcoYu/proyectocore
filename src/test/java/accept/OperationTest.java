package accept;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Operacion;

public class OperationTest {

	private Operacion operation;
	
	@Test
	public void sumarTest() {
		
		operation = new Operacion(5, 4);
		
		assertEquals(operation.sumar(), 9);
	}
}
